// variables du jeu

let words = [
    "examen",
    "javascript",
    "benjamin",
    "innerhtml",
    "document",
    "html",
    "css",
    "script",
    "adrec",
    "framework",
    "bioshock",
    "corde",
    "tabouret",
    "pendu"
];

let answer = ''; // mot a deviner
let maxError = 10; // nombre d'essais
let mistakes = 0; // nombre d'erreurs
let guessed = []; // lettres devinées
let wordStatus = null; // mot en cours

// definir le mot a trouver aleatoirement

function randomWord() {
    answer = words[Math.floor(Math.random() * words.length)];
}

// afficher les boutons dans le html

function generateButtons() {
    let buttonsInHTML = 'abcdefghijklmnopqrstuvwxyz'.split('').map(letter =>
        `
      <button
        class="btn"
        id='` + letter + `'
        onClick="handleGuess('` + letter + `')"
      >
        ` + letter + `
      </button>
    `).join('');

    document.getElementById('keyboard').innerHTML = buttonsInHTML;
}

// clic des lettres bonne / mauvaise rep

function handleGuess(chosenLetter) {
    guessed.indexOf(chosenLetter) === -1 ? guessed.push(chosenLetter) : null;
    document.getElementById(chosenLetter).setAttribute('disabled', true);

    if (answer.indexOf(chosenLetter) >= 0) {
        guessedWord();
        checkIfWon();
    } else if (answer.indexOf(chosenLetter) === -1) {
        mistakes++;
        updateErrors();
        checkIfLost();
        updateHangmanPicture();
    }
}

// animation de l'image

function updateHangmanPicture() {
    document.getElementById('imagesPendu').src = 'imagesPendu/' + mistakes + '.png' ;
}

// condition de victoire

function checkIfWon() {
    if (wordStatus === answer) {
        document.getElementById('keyboard').innerHTML = 'Vous echappez à la corde!!!';
        document.getElementById('imagesPendu').src = 'imagesPendu/start.png';
    }
}
// condition de defaite

function checkIfLost() {
    if (mistakes === maxError) {
        document.getElementById('wordSpotlight').innerHTML = 'La reponse etait: ' + answer;
        document.getElementById('keyboard').innerHTML = 'Vous vous balancez au bout de la corde';
    }
}
// avancement du mot en cours

function guessedWord() {
    wordStatus = answer.split('').map(letter => (guessed.indexOf(letter) >= 0 ? letter : " _ ")).join('');

    document.getElementById('wordSpotlight').innerHTML = wordStatus;
}

// mise a jour des erreurs

function updateErrors() {
    document.getElementById('mistakes').innerHTML = mistakes;
}

// redemarrage de la partie

function reset() {
    mistakes = 0;
    guessed = [];
    document.getElementById('imagesPendu').src = 'imagesPendu/start.png';
    randomWord();
    guessedWord();
    updateErrors();
    generateButtons();
}
document.getElementById('maxWrong').innerHTML = maxError;
randomWord();
generateButtons();
guessedWord();